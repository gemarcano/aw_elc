// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#include <iostream>
#include "hidapi.h"
#include "aw_elc.h"

int main()
{
	aw_elc::hidapi &hidapi = aw_elc::hidapi::get();
	aw_elc::hid_info_unique_ptr infos = hidapi.enumerate(0x187c, 0x0550);
	if (!infos)
	{
		std::cerr << "Did not find any AW_ELC controllers" << std::endl;
		return -1;
	}

	std::cout << "Using first controller at: " << infos->path << std::endl;

	aw_elc::hid_dev_unique_ptr dev = hidapi.open(*infos);
	aw_elc::aw_elc_controller controller(std::move(dev), *infos);

	// Clean up whatever animations there are in the default slots
	controller.remove_animation(0x0008);
	controller.remove_animation(0x0061);

	// Restore the first animation-- this should be the one that plays on boot
	controller.save_animation(0x0008, std::map<std::vector<uint8_t>, decltype(aw_elc::rainbow[0])>{
			{{0,1,2,3}, aw_elc::rainbow[0]},
	});

	// This should be the second, a teal-ish color that is transitioned to after boot
	auto color_template = aw_elc::static_color;
	color_template.color = aw_elc::rgb_color(0x00F0F0);
	color_template.period = 1000;
	color_template.tempo = 100;

	controller.save_animation(0x0061, std::map<std::vector<uint8_t>, std::array<decltype(color_template), 1>>{
			{{0,1,2,3}, {color_template}},
	});
	controller.set_default(0x0061);
	controller.set_startup(0x0008, 6000);

	return 0;
}
