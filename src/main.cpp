// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#include <iostream>
#include "hidapi.h"
#include "aw_elc.h"

int main()
{
	aw_elc::hidapi &hidapi = aw_elc::hidapi::get();
	aw_elc::hid_info_unique_ptr infos = hidapi.enumerate(0x187c, 0x0550);
	if (!infos)
	{
		std::cerr << "Did not find any AW_ELC controllers" << std::endl;
		return -1;
	}

	std::cout << "Using first controller at: " << infos->path << std::endl;

	aw_elc::hid_dev_unique_ptr dev = hidapi.open(*infos);
	aw_elc::aw_elc_controller controller(std::move(dev), *infos);
	unsigned dim = 0;
	std::vector<uint8_t> zones{{0,1,2,3}};
	std::cout << "Setting dim to " << dim << " for zones [";
	for (size_t i = 0; i < zones.size(); ++i)
	{
		std::cout << static_cast<unsigned>(zones[i]);
		if (i != (zones.size()-1))
			std::cout << ", ";
	}
	std::cout << "]" << std::endl;

	controller.set_dim(zones, dim);

	/*controller.play_animation(std::map<std::vector<uint8_t>, decltype(aw_elc::rainbow[0])>{
			{{0}, aw_elc::rainbow[0]},
			{{1}, aw_elc::rainbow[1]},
			{{2}, aw_elc::rainbow[2]},
			{{3}, aw_elc::rainbow[3]},
	});*/
	controller.play_animation(0x0008);
	//controller.play_animation(0x0061);
	return 0;
}
