// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#include "hidapi.h"
#include "aw_elc.h"

#include <iostream>
#include <filesystem>
#include <fstream>
#include <algorithm>

int main()
{
	aw_elc::hidapi &hidapi = aw_elc::hidapi::get();
	aw_elc::hid_info_unique_ptr infos = hidapi.enumerate(0x187c, 0x0550);
	if (!infos)
	{
		std::cerr << "Did not find any AW_ELC controllers" << std::endl;
		return -1;
	}

	namespace fs = std::filesystem;
	fs::path home{std::getenv("HOME")};
	fs::path dir{home/".config/aw_elc/"};
	fs::path name{"dim"};
	std::error_code er{};
	if (!fs::is_directory(dir))
		fs::create_directories(dir, er);
	if (er)
	{
		std::cerr << "Unable to create directory " << dir << std::endl;
		return -1;
	}
	// FIXME I have some redundant logic between here and levels handling
	if (!fs::is_regular_file(dir/name))
	{
		std::ofstream dim_file(dir/name);
		if (!dim_file)
			return -1;
		dim_file << 100 << ' ' << 50 << ' ' << 0 << std::endl;
	}

	std::vector<unsigned> levels;
	{
		std::ifstream dim_file(dir/name);
		if (!dim_file)
			return -1;
		while (dim_file) {
			unsigned current = 0;
			dim_file >> current;
			if (!dim_file)
				break;
			levels.emplace_back(current);
		}
	}
	for (auto level: levels)
	{
		if (level > 100)
		{
			levels.clear();
			levels = std::vector<unsigned>{0, 50, 100};
			break;
		}
	}

	std::ofstream dim_file(dir/name);
	if (!dim_file)
		return -1;

	std::cout << "Using first controller at: " << infos->path << std::endl;

	aw_elc::hid_dev_unique_ptr dev = hidapi.open(*infos);
	aw_elc::aw_elc_controller controller(std::move(dev), *infos);
	unsigned dim = levels[0];
	std::rotate(levels.begin(), levels.begin()+1, levels.end());

	std::vector<uint8_t> zones{{0,1,2,3}};
	std::cout << "Setting dim to " << dim << " for zones [";
	for (size_t i = 0; i < zones.size(); ++i)
	{
		std::cout << static_cast<unsigned>(zones[i]);
		if (i != (zones.size()-1))
			std::cout << ", ";
	}
	std::cout << "]" << std::endl;

	controller.set_dim(zones, dim);
	for (size_t i = 0; i < levels.size() - 1; ++i)
		dim_file << levels[i] << ' ';
	dim_file << levels[levels.size()-1]  << std::endl;
	return 0;
}
