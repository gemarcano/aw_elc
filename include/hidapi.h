// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#ifndef AW_ELC_HIDAPI_H_
#define AW_ELC_HIDAPI_H_

#include <hidapi/hidapi.h>
#include <memory>

namespace aw_elc
{
	struct hid_info_deleter
	{
		void operator()(hid_device_info *info)
		{
			hid_free_enumeration(info);
		}
	};

	struct hid_dev_deleter
	{
		void operator()(hid_device *dev)
		{
			hid_close(dev);
		}
	};

	// FIXME these are only valid for the duration of the HID sesssion. Maybe
	// info and dev should be wrapped in some object that keeps a shared_ptr of
	// the main hidapi instance to act as a reference counter???
	using hid_info_unique_ptr = std::unique_ptr<hid_device_info, hid_info_deleter>;
	using hid_dev_unique_ptr = std::unique_ptr<hid_device, hid_dev_deleter>;

	class hidapi
	{
	public:
		static hidapi& get()
		{
			static hidapi instance;
			return instance;
		}

		hid_info_unique_ptr enumerate(uint16_t vid, uint16_t pid)
		{
			return hid_info_unique_ptr(hid_enumerate(vid, pid));
		}

		hid_dev_unique_ptr open(const hid_device_info& info)
		{
			return hid_dev_unique_ptr(hid_open(info.vendor_id, info.product_id, info.serial_number));
		}

		~hidapi()
		{
			hid_exit();
		}

	private:
		hidapi()
		{
			hid_init();
		}

	};
}
#endif//AW_ELC_HIDAPI_H_
