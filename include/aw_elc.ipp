// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#ifndef AW_ELC_IPP_
#define AW_ELC_IPP_

#include <hidapi/hidapi.h>

#include <string>
#include <array>
#include <cstdint>
#include <vector>
#include <map>
#include <chrono>
#include <thread>

namespace aw_elc
{
	/** Sends an HID report, and rate-limits reports to deal with controller
	 * idiocy (RGB controller can't handle too many requests at once).
	 *
	 * @tparam Data Container with output bytes of report to send out, must be
	 *  contiguous in memory.
	 *
	 * @param[in,out] dev USB device to send report to.
	 * @param[in] output_report Report in a container to send to the USB
	 *  device.
	 */
	template<class Data>
	void send_hid_report(
		hid_dev_unique_ptr& dev, const Data& output_report)
	{
		using namespace std::chrono_literals;
		hid_send_feature_report(dev.get(), &output_report[0], output_report.size() * sizeof(output_report[0]));
		// The controller really doesn't like really spammed by too many commands
		// at once... the delay may be command dependent also
		// Delay for longer if the command is changing animation state
		auto command = controller::command(output_report[2]);
		auto subcommand = controller::anim_command(output_report[3]);
		if (command == controller::command::USER_ANIM &&
				(subcommand == controller::anim_command::FINISH_PLAY ||
				 subcommand == controller::anim_command::FINISH_SAVE))
		{
			std::this_thread::sleep_for(1s);
		}
		else
		{
			std::this_thread::sleep_for(60ms);
		}
	}

	/** Play the animation described by the argument.
	 *
	 * @tparam C1 Container of zones to apply animation_step objects to
	 * @tparam C2 Container of animation_step objects to send
	 *
	 * @param[in] steps Map correlating zones to animation_step objects to send
	 * to the controller and to play.
	 *
	 * @returns True on success, false otherwise.
	 */
	template<class C1, class C2>
	bool aw_elc_controller::play_animation(const std::map<C1, C2>& steps)
	{
		bool result = true;
		result &= user_animation(controller::anim_command::NEW, controller::animation::KEYBOARD, 0);
		for (auto& pair : steps)
		{
			result &= select_zones(pair.first);
			result &= mode_action(pair.second);
		}
		result &= user_animation(controller::anim_command::FINISH_PLAY, controller::animation::KEYBOARD, 0);
		return result;
	}

	/** Save the animation described by the argument.
	 *
	 * @tparam C1 Container of zones to apply animation_step objects to
	 * @tparam C2 Container of animation_step objects to send
	 *
	 * @param[in] anim Animation index to save to.
	 * @param[in] steps Map correlating zones to animation_step objects to send
	 * to the controller and to save.
	 *
	 * @returns True on success, false otherwise.
	 */
	template<class C1, class C2>
	bool aw_elc_controller::save_animation(uint16_t anim, const std::map<C1, C2>& steps)
	{
		bool result = true;
		result &= user_animation(controller::anim_command::NEW, anim, 0);
		for (auto& pair : steps)
		{
			result &= select_zones(pair.first);
			result &= mode_action(pair.second);
		}
		result &= user_animation(controller::anim_command::FINISH_SAVE, controller::animation::KEYBOARD, 0);
		return result;
	}

	/** Sends a series of animation_step objects to the controller.
	 *
	 * @tparam Container Container of animation_step objects.
	 *
	 * @param[in] steps Series of animation_step objects to send.
	 *
	 * @returns True on success, false otherwise.
	 */
	template<class Container>
	bool aw_elc_controller::mode_action(const Container& steps)
	{
		std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

		// Single configuration, set first byte to 0
		output_report[0x00] = 0x00;
		output_report[0x01] = 0x03;
		output_report[0x02] = static_cast<unsigned char>(controller::command::ADD_ACTION);

		size_t left = steps.size();
		bool result = true;
		auto step = steps.begin();
		while (left && result)
		{
			size_t amount = std::min<size_t>(left, 3u);
			for (unsigned i = 0; i < amount; ++i)
			{
				output_report[0x03 + 8*i] = static_cast<unsigned char>(step->mode);
				output_report[0x04 + 8*i] = static_cast<uint16_t>(step->period) >> 8;
				output_report[0x05 + 8*i] = static_cast<uint16_t>(step->period) & 0xFF;
				output_report[0x06 + 8*i] = step->tempo >> 8;
				output_report[0x07 + 8*i] = step->tempo & 0xFF;
				output_report[0x08 + 8*i] = step->color.R();
				output_report[0x09 + 8*i] = step->color.G();
				output_report[0x0A + 8*i] = step->color.B();
				step = std::next(step);
			}
			send_hid_report(dev_, output_report);
			auto response = get_response();
			static_assert(response.size() == sizeof(output_report), "Mismatched size");
			// For this command, error is if the output equals the input
			result &= response[1] == 0x03 && output_report != response;

			left -= amount;
		}

		return result;
	}
}

#endif//AW_ELC_IPP_
