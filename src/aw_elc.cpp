// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#include "aw_elc.h"
#include "hidapi.h"

#include <map>
#include <thread>
#include <chrono>
#include <sstream>
#include <cstring>
#include <cstdint>
#include <algorithm>

using namespace aw_elc;

using alienware_platform_id = uint32_t;

// Some devices appear to report the wrong number of zones. Record that here.
static const std::map<alienware_platform_id, uint8_t> zone_quirks = {
	{0x0c01, 4} // Dell G5 SE 5505
};

aw_elc_controller::aw_elc_controller(
	hid_dev_unique_ptr&& device, const hid_device_info& info)
: dev_(std::move(device)), location_(info.path)
{
	std::wstring serial_number = info.serial_number;
	serial_number_ = std::string(serial_number.begin(), serial_number.end());

	// Get zone information by checking firmware configuration
	auto data = report(controller::report::CONFIG);
	alienware_platform_id platform_id = data[4] << 8 | data[5];

	// Get fw version
	data = report(controller::report::FIRMWARE);
	std::stringstream ss;
	ss << static_cast<unsigned>(data[4]) << '.' <<
		static_cast<unsigned>(data[5]) << '.' << static_cast<unsigned>(data[6]);
	version_ = ss.str();

	zone_count_ = zone_quirks.count(platform_id) ? zone_quirks.at(platform_id) : data[6];
}

unsigned int aw_elc_controller::get_zone_count() const
{
	return zone_count_;
}

std::string aw_elc_controller::get_location() const
{
	return location_;
}

std::string aw_elc_controller::get_serial() const
{
	return serial_number_;
}

std::string aw_elc_controller::get_firmware_version() const
{
	return version_;
}

aw_elc_controller::hidapi_aw_elc_report aw_elc_controller::get_response()
{
	// Zero init. This is not updated if there's a problem.
	hidapi_aw_elc_report result{};
	hid_get_feature_report(dev_.get(), result.data(), result.size());
	return result;
}

aw_elc_controller::hidapi_aw_elc_report aw_elc_controller::report(
	controller::report subcommand)
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::REPORT);
	output_report[0x03] = static_cast<unsigned char>(subcommand);

	send_hid_report(dev_, output_report);

	return get_response();
}

aw_elc_report aw_elc_controller::get_status(controller::report subcommand)
{
	auto data = report(subcommand);
	auto result = aw_elc_report{};
	static_assert(data.size() == HIDAPI_AW_ELC_REPORT_SIZE &&
		result.size() == (data.size() - 1), "unexpected sizes");
	// Skip first byte, as that's the report number, which should be 0
	std::copy(data.begin() + 1, data.end(), result.begin());
	return result;
}

bool aw_elc_controller::set_dim(
	std::vector<uint8_t> zones, uint8_t percent)
{
	// Bail out if there are no zones to update
	if (!zones.size())
		return true;

	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::DIM);
	output_report[0x03] = static_cast<unsigned char>(percent);
	uint16_t num_zones = zones.size();
	output_report[0x04] = num_zones >> 8;;
	output_report[0x05] = num_zones & 0xFF;
	for (size_t i = 0; i < num_zones; ++i)
	{
		output_report[0x06+i] = zones[i];
	}

	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;
}

bool aw_elc_controller::user_animation(
	controller::anim_command subcommand, uint16_t animation, uint16_t duration)
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::USER_ANIM);
	output_report[0x03] = static_cast<uint16_t>(subcommand) >> 8;
	output_report[0x04] = static_cast<uint16_t>(subcommand) & 0xFF;
	output_report[0x05] = static_cast<uint16_t>(animation) >> 8;
	output_report[0x06] = static_cast<uint16_t>(animation) & 0xFF;
	output_report[0x07] = duration >> 8;
	output_report[0x08] = duration & 0xFF;

	send_hid_report(dev_, output_report);

	// Every subcommand appears to report its result on a different byte
	auto response = get_response();

	// The only time the 0x03 byte is zero is if the controller has crashed
	if (response[1] == 0)
		return false;

	switch (subcommand)
	{
	case controller::anim_command::FINISH_SAVE:
		return !response[7];
	case controller::anim_command::FINISH_PLAY:
		return !response[5];
	case controller::anim_command::PLAY:
		return !response[7];
	default:
		return true;
	}
}

bool aw_elc_controller::select_zones(
	const std::vector<uint8_t>& zones)
{
	// Bail if zones is empty, and return false to indicate nothing has changed
	if (!zones.size())
		return false;

	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::SELECT_ZONES);
	output_report[0x03] = 1; // loop?
	uint16_t num_zones = zones.size();
	output_report[0x04] = num_zones >> 8;
	output_report[0x05] = num_zones & 0xFF;
	for (size_t i = 0; i < num_zones; ++i)
	{
		output_report[0x06+i] = zones[i];
	}

	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;
}

bool aw_elc_controller::remove_animation(uint16_t anim)
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::USER_ANIM);

	output_report[0x03] = static_cast<uint16_t>(controller::anim_command::REMOVE) >> 8;
	output_report[0x04] = static_cast<uint16_t>(controller::anim_command::REMOVE) & 0xFF;
	output_report[0x05] = static_cast<uint16_t>(anim) >> 8;
	output_report[0x06] = static_cast<uint16_t>(anim) & 0xFF;
	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;
}

bool aw_elc_controller::play_animation(uint16_t anim)
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::USER_ANIM);

	output_report[0x03] = static_cast<uint16_t>(controller::anim_command::PLAY) >> 8;
	output_report[0x04] = static_cast<uint16_t>(controller::anim_command::PLAY) & 0xFF;
	output_report[0x05] = static_cast<uint16_t>(anim) >> 8;
	output_report[0x06] = static_cast<uint16_t>(anim) & 0xFF;
	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;

}

bool aw_elc_controller::set_default(uint16_t anim)
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::USER_ANIM);

	output_report[0x03] = static_cast<uint16_t>(controller::anim_command::DEFAULT) >> 8;
	output_report[0x04] = static_cast<uint16_t>(controller::anim_command::DEFAULT) & 0xFF;
	output_report[0x05] = static_cast<uint16_t>(anim) >> 8;
	output_report[0x06] = static_cast<uint16_t>(anim) & 0xFF;
	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;
}

bool aw_elc_controller::set_startup(uint16_t anim, uint16_t duration)
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::USER_ANIM);

	output_report[0x03] = static_cast<uint16_t>(controller::anim_command::STARTUP) >> 8;
	output_report[0x04] = static_cast<uint16_t>(controller::anim_command::STARTUP) & 0xFF;
	output_report[0x05] = static_cast<uint16_t>(anim) >> 8;
	output_report[0x06] = static_cast<uint16_t>(anim) & 0xFF;
	output_report[0x07] = static_cast<uint16_t>(duration) >> 8;
	output_report[0x08] = static_cast<uint16_t>(duration) & 0xFF;
	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;
}

bool aw_elc_controller::set_color_direct(
	rgb_color color,
	std::vector<uint8_t> zones)
{
	// Bail if zones is empty
	if (zones.empty())
		return true;

	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::SET_COLOR);

	output_report[0x03] = color.R();
	output_report[0x04] = color.G();
	output_report[0x05] = color.B();
	uint16_t num_zones = zones.size();
	output_report[0x06] = static_cast<uint16_t>(num_zones) >> 8;
	output_report[0x07] = static_cast<uint16_t>(num_zones) & 0xFF;
	for (size_t i = 0; i < num_zones; ++i)
	{
		output_report[0x08+i] = zones[i];
	}

	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == sizeof(output_report), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03 && output_report != response;
}

bool aw_elc_controller::reset()
{
	std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> output_report{};

	// Single configuration, set first byte to 0
	output_report[0x00] = 0x00;
	output_report[0x01] = 0x03;
	output_report[0x02] = static_cast<unsigned char>(controller::command::RESET);

	send_hid_report(dev_, output_report);

	auto response = get_response();
	static_assert(response.size() == output_report.size(), "Mismatched size");
	// For this command, error is if the output equals the input
	return response[1] == 0x03;
}
