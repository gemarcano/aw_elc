// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#ifndef AW_ELC_H_
#define AW_ELC_H_

#include "hidapi.h"

#include <map>
#include <string>
#include <array>
#include <vector>
#include <chrono>
#include <thread>
#include <cstdint>

namespace aw_elc
{
	namespace controller
	{
		enum class command : uint8_t
		{
			REPORT       = 0x20,   // Set report type to get
			USER_ANIM    = 0x21,   // Set user animation settings
			POWER_ANIM   = 0x22,   // Set power animation settings
			SELECT_ZONES = 0x23,   // Select zones to apply actions to
			ADD_ACTION   = 0x24,   // Set actions to apply
			UNKNOWN1     = 0x25,   // Supposedly set event?
			DIM          = 0x26,   // Set dim percentage
			SET_COLOR    = 0x27,   // Unclear (causes color flash)
			RESET        = 0x28,   // Reset
			ERASE_FLASH  = 0xff,   // Erases flash memory on controller
		};

		enum class report : uint8_t
		{
			FIRMWARE     = 0x00,   // Get firmware verion
			STATUS       = 0x01,   // Get status
			CONFIG       = 0x02,   // Get firmware config
			ANIMATION    = 0x03,   // Get animation count and last id
			UNKNOWN1     = 0x04,   // Get ELC animation by ID
			UNKNOWN2     = 0x05,   // Read series???
			UNKNOWN3     = 0x06,   // Get action???
			UNKNOWN4     = 0x07,   // Get Caldera status???
		};

		enum class anim_command : uint16_t
		{
			NEW          = 0x0001, // Start new animation
			FINISH_SAVE  = 0x0002, // Finish and save animation
			FINISH_PLAY  = 0x0003, // Finish and play animation
			REMOVE       = 0x0004, // Remove/erase animation
			PLAY         = 0x0005, // Play animation
			DEFAULT      = 0x0006, // Set default animation
			STARTUP      = 0x0007, // Set startup animation
		};

		namespace animation
		{
			constexpr uint16_t DEFAULT_STARTUP = 0x0008; // Default slot for startup
			constexpr uint16_t DEFAULT         = 0x0061; // Default slot
			constexpr uint16_t KEYBOARD        = 0xFFFF; // Non-saved animation slot
		}

		enum class mode : uint8_t
		{
			COLOR        = 0x00,   // Action to set color mode
			PULSE        = 0x01,   // Action to set pulse mode
			MORPH        = 0x02,   // Action to set morph mode
			SPECTRUM,    // Abitrary code for spectrum mode
			RAINBOW,     // Arbitrary code for rainbow wave mode
			BREATHING,   // Arbitrary code for rainbow wave mode
		};

		namespace tempo
		{
			constexpr uint16_t MIN = 0x0064;  // Min tempo (as used by AWCC)
			constexpr uint16_t MAX = 0x00fa;  // Max tempo (as used by AWCC)
			constexpr uint16_t SPECTRUM = 0x000f;  // Used by Spectrum mode
		}

		namespace duration
		{
			constexpr uint16_t LONG     = 0x09c4;  // Min tempo (as used by AWCC)
			constexpr uint16_t MED      = 0x05dc;  // Max tempo (as used by AWCC)
			constexpr uint16_t SHORT    = 0x01f3;  // Max tempo (as used by AWCC)
			constexpr uint16_t SPECTRUM = 0x01ac;  // Used by Spectrum mode
		}
	}

	using aw_elc_report = std::array<unsigned char, 33>;

	class rgb_color
	{
	public:
		constexpr rgb_color()
		:color_(0)
		{}

		constexpr rgb_color(uint8_t R, uint8_t G, uint8_t B)
		:color_(R << 16 | G << 8 | B)
		{}

		constexpr rgb_color(uint32_t RGB)
		:color_(RGB)
		{}

		constexpr auto operator<=>(const rgb_color& color) const = default;

		constexpr uint8_t R() const { return color_ >> 16; }
		constexpr uint8_t G() const { return (color_ >> 8) & 0xFF; }
		constexpr uint8_t B() const { return color_ & 0xFF; }

		constexpr uint32_t RGB() const { return color_; }

	private:
		uint32_t color_;
	};

	struct animation_step
	{
		constexpr animation_step(controller::mode mode, rgb_color color, uint16_t period, uint16_t tempo)
		:mode(mode), color(color), period(period), tempo(tempo)
		{}

		controller::mode mode;
		rgb_color color;
		uint16_t period;
		uint16_t tempo;
	};


	class aw_elc_controller
	{
	public:
		aw_elc_controller(hid_dev_unique_ptr&& dev, const hid_device_info& info);

		std::string get_serial() const;
		std::string get_firmware_version() const;
		unsigned get_zone_count() const;
		std::string get_location() const;

		bool remove_animation(uint16_t anim);
		bool play_animation(uint16_t anim);
		bool set_dim(std::vector<uint8_t> zones, uint8_t percent);
		aw_elc_report get_status(controller::report subcommand);

		template<class C1, class C2>
		bool play_animation(const std::map<C1, C2>& steps);

		template<class C1, class C2>
		bool save_animation(uint16_t anim, const std::map<C1, C2>& steps);

		bool set_default(uint16_t anim);
		// Duration of 0 uses whatever duration was there before
		// Interestingly enough, if the duration is longer than the time it
		// takes for the keyboard to go to sleep, the duration counter appears
		// to stop while the lights are off, and picks up where it was when
		// they light up again.
		bool set_startup(uint16_t anim, uint16_t duration);

	private:
		hid_dev_unique_ptr dev_;
		std::string device_name_;
		std::string location_;
		std::string serial_number_;
		std::string version_;
		unsigned zone_count_;

		static constexpr int HIDAPI_AW_ELC_REPORT_SIZE = 34;
		typedef std::array<unsigned char, HIDAPI_AW_ELC_REPORT_SIZE> hidapi_aw_elc_report;

		hidapi_aw_elc_report get_response();
		hidapi_aw_elc_report report(controller::report subcommand);
		bool select_zones(const std::vector<uint8_t>& zones);
		bool user_animation(controller::anim_command subcommand, uint16_t animation, uint16_t duration);
		template<class Container>
		bool mode_action(const Container& steps);

		bool set_color_direct(rgb_color color, std::vector<uint8_t> zones);
		bool update_direct();
		bool reset();
	};

	constexpr animation_step static_color({controller::mode::COLOR, 0, 2000, controller::tempo::MAX});
	constexpr animation_step pulse_color({controller::mode::PULSE, 0, 2000, controller::tempo::MAX});
	constexpr std::array<animation_step, 2> morph_color =
	{
		animation_step{controller::mode::MORPH, 0, 2000, controller::tempo::MAX},
		{controller::mode::MORPH, 0, 2000, controller::tempo::MAX},
	};

	constexpr std::array<std::array<rgb_color, 7>, 4> rainbow_colors
	{{
		{ 0xFF0000, 0xFFA500, 0xFFFF00, 0x008000, 0x00BFFF, 0x0000FF, 0x800080 },
		{ 0x800080, 0xFF0000, 0xFFA500, 0xFFFF00, 0x008000, 0x00BFFF, 0x0000FF },
		{ 0x0000FF, 0x800080, 0xFF0000, 0xFFA500, 0xFFFF00, 0x008000, 0x00BFFF },
		{ 0x00BFFF, 0x0000FF, 0x800080, 0xFF0000, 0xFFA500, 0xFFFF00, 0x008000 }
	}};


	constexpr std::array<animation_step, 7> spectrum_color =
	{
		animation_step{controller::mode::MORPH, rainbow_colors[0][0], 2000, controller::tempo::SPECTRUM},
		{controller::mode::MORPH, rainbow_colors[0][1], 2000, controller::tempo::SPECTRUM},
		{controller::mode::MORPH, rainbow_colors[0][2], 2000, controller::tempo::SPECTRUM},
		{controller::mode::MORPH, rainbow_colors[0][3], 2000, controller::tempo::SPECTRUM},
		{controller::mode::MORPH, rainbow_colors[0][4], 2000, controller::tempo::SPECTRUM},
		{controller::mode::MORPH, rainbow_colors[0][5], 2000, controller::tempo::SPECTRUM},
		{controller::mode::MORPH, rainbow_colors[0][6], 2000, controller::tempo::SPECTRUM},
	};

	constexpr std::array<std::array<animation_step, 7>, 4> rainbow {
		std::array<animation_step, 7>
		{
			animation_step{controller::mode::MORPH, rainbow_colors[0][0], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[0][1], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[0][2], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[0][3], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[0][4], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[0][5], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[0][6], 0x282, controller::tempo::SPECTRUM},
		},
		{
			animation_step{controller::mode::MORPH, rainbow_colors[1][0], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[1][1], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[1][2], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[1][3], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[1][4], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[1][5], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[1][6], 0x282, controller::tempo::SPECTRUM},
		},
		{
			animation_step{controller::mode::MORPH, rainbow_colors[2][0], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[2][1], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[2][2], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[2][3], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[2][4], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[2][5], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[2][6], 0x282, controller::tempo::SPECTRUM},
		},
		{
			animation_step{controller::mode::MORPH, rainbow_colors[3][0], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[3][1], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[3][2], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[3][3], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[3][4], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[3][5], 0x282, controller::tempo::SPECTRUM},
			{controller::mode::MORPH, rainbow_colors[3][6], 0x282, controller::tempo::SPECTRUM},
		},
	};
	// Breathing is morph is 0 as second color
}

#include "aw_elc.ipp"

#endif//AW_ELC_H_
