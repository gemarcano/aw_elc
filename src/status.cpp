// SPDX-FileCopyrightText: 2021 Gabriel Marcano <gabemarcano@yahoo.com>
// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later

#include "aw_elc.h"
#include "hidapi.h"

#include <iostream>
#include <iomanip>

int main()
{
	aw_elc::hidapi &hidapi = aw_elc::hidapi::get();
	aw_elc::hid_info_unique_ptr infos = hidapi.enumerate(0x187c, 0x0550);
	if (!infos)
	{
		std::cerr << "Did not find any AW_ELC controllers" << std::endl;
		return -1;
	}

	for (auto* info = infos.get(); info; info = info->next)
	{
		std::cout << "Printing status information for: " << info->path << std::endl;
		aw_elc::hid_dev_unique_ptr dev = hidapi.open(*info);
		aw_elc::aw_elc_controller controller(std::move(dev), *info);
		for (size_t i = 0; i < 8; ++i)
		{
			auto report = controller.get_status(aw_elc::controller::report(i));
			std::cout << "STATUS " << std::setw(2) << std::setfill('0') << std::hex << i << ": ";
			for (size_t j = 0; j < report.size(); ++j)
			{
				std::cout << std::hex << std::setfill('0') << std::setw(2) << (int)report[j] << " ";
			}
			std::cout << std::endl;
		}
	}

	return 0;
}
